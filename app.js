const express = require("express");
var cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json());

app.post("/api/login", (req, res) => {
  const body = req.body;

  const user = {
    username: "example_user",
    password: "example_password",
  };

  const { username, password } = user;

  if (body.username === username && body.password === password) {
    res.json({
      success: true,
      status: 200,
      message: "Authorized.",
    });
  } else {
    res.json({
      success: false,
      status: 401,
      message: "Unauthorized.",
    });
  }
});

app.listen(3000, () => {
  console.log("Server is running on port 3000");
});
