FROM node:16.17.0-bullseye-slim AS development

WORKDIR /app
 
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json

RUN npm ci

COPY . /app

CMD [ "node", "app.js" ]

EXPOSE 3000